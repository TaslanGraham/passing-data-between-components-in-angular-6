import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-test",
  templateUrl: "./test.component.html",
  styleUrls: ["./test.component.css"]
})
export class TestComponent implements OnInit {
  @Input("Data") public name; // receives the Data variable been passed over by the App component and stores it in a variable called name

  /*
  to send data from the child component (test) to the paren(App)
  we use events via an EventEmitter() object (childEvent). 
  So, on a given event( example click) we assigned some data 
  childEvent and emit(basically expose it as an event) it
  emit() allows the event to be captured in the parent class
  */
  @Output() public childEvent = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  fireEvent() {
    this.childEvent.emit("Hey genius"); // expose the event
  }
}
